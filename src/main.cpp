#include <libpq-fe.h>
#include <iostream>
#include <cstring> //pour les chaine de caracteres
#include <iomanip> // pour setw

#include "Affichage.h"
#include "gestion_Donnee.h"

enum CODES_ERREUR
{
  OK_TOUT_VA_BIEN,
  ERR_NO_PING,
  ERR_NO_CONNECTION
};


int main()
{
  int code_retour;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=c.lemeur user=c.lemeur password=P@ssword";
  PGPing code_retour_ping;
  PGconn *connexion;
  code_retour = OK_TOUT_VA_BIEN;
  code_retour_ping = PQping(info_connexion);

  if(code_retour_ping == PQPING_OK)
  {
    //connection au serveur ou affichage des erreurs
    connexion = PQconnectdb(info_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec les paramètres suivants : " << std::endl;
      Affichage_Info(connexion);
      Gestion_Donnee(connexion);
    }
    else
    {
      std::cerr << "Erreurs de connexion" << std::endl;
    }
  }
  else
  {
    std::cerr << "Le serveur n'est pas accessible pour le moment veuillez contacter votre administrateur réseau" << std::endl;
  }

  return code_retour;
}
