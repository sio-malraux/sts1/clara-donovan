#include <libpq-fe.h>
#include <string>

void Affichage_Info(PGconn *_connexion);
void Affichage_Ligne(int taille, int nb);
std::string Affichage_Donnee(char *info, unsigned int taille);
