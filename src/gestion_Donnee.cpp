#include "gestion_Donnee.h"
#include "Affichage.h"
#include <iostream>
#include <iomanip>
#include <cstring>

void Gestion_Donnee(PGconn *_connexion)
{
  int i = 0, j, k, nbColonnes, nbLignes, ligne, col, taille = 0;
  PGresult *res = PQexec(_connexion, "SET SCHEMA 'si6'; SELECT \"Animal\".id,\"Animal\".nom,sexe,date_naissance AS \"date de naissance\", commentaires,\"Race\".nom as race, description FROM \"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura';");

  if(PQresultStatus(res) == PGRES_TUPLES_OK)
  {
    /* affiche le noms des colonnes */
    nbColonnes = PQnfields(res);
    nbLignes = PQntuples(res);

    for(ligne = 0; ligne < nbLignes; ligne++)
    {
      for(col = 0; col < nbColonnes; col++)
      {
        //on regarde quelles titres parmi les colonnes est le grand
        int titre = std::strlen(PQfname(res, col));

        if(titre > taille)
        {
          taille = titre;
        }

        int length = PQgetlength(res, ligne, col);

        //std::cout << length << " - ";
        //on regarde quelles titres parmi les colonnes est le grand
        if(length < 30 && length > taille)
        {
          taille = length;
        }
      }
    }

    //std::cout << "VARIABLE :" << taille << std::endl;
    Affichage_Ligne(taille, nbColonnes);

    for(i = 0; i < nbColonnes; i++)
    {
      std::setiosflags(std::ios::left);
      std::cout << "| " << std::left << std::setw(taille) << PQfname(res, i) << " " ;
    }

    std::cout << "|" << std::endl ;
    Affichage_Ligne(taille, nbColonnes);

    // affiche les lignes normaliser
    for(j = 0; j < nbLignes; j++)
    {
      for(k = 0; k < nbColonnes; k++)
      {
        std::cout << "| " << std::setw(taille) << Affichage_Donnee(PQgetvalue(res, j, k), taille) << " ";
      }

      std::cout << "|" << std::endl;
    }

    Affichage_Ligne(taille, nbColonnes);
    std::cout << "\nL’exécution de la requête SQL a retourné " << nbLignes << " enregistrements ." << std::endl;
  }
  else
  {
    std::cerr << "\tERREUR" << std::endl;
  }

  PQclear(res);
}
