#include "Affichage.h"
#include <iostream>
#include <iomanip>
#include <cstring>

void Affichage_Info(PGconn *_connexion)
{
  std::cout << " * utilisateur : " << PQuser(_connexion) << std::endl;
  std::cout << " * mot de passe : " ;
  //intialisation d'une valeur longueur du mdp
  int l = std::strlen(PQpass(_connexion));

  for(int i = 0; i < l; ++i)
  {
    std::cout << "*";
  }

  std::cout << std::endl;
  std::cout << " * base de données : " << PQdb(_connexion) << std::endl;
  std::cout << " * port TCP : " << PQport(_connexion) << std::endl;

  if(PQsslInUse(_connexion) == 1)
  {
    std::cout << " * chiffrage SSL : true" << std::endl;
  }
  else
  {
    std::cout << " * chiffrage SSL : false" << std::endl;
  }

  std::cout << " * encodage : " << PQparameterStatus(_connexion, "server_encoding") << std::endl;
  std::cout << " * version du protocole : " << PQprotocolVersion(_connexion) << std::endl;
  std::cout << " * version du serveur : " << PQserverVersion(_connexion) << std::endl;
  std::cout << " * version de la bibliothèque ’libpq’ du client : " << PQlibVersion() << std::endl;
}

void Affichage_Ligne(int taille, int nb)
{
  // std::cout << std::setfill('-') << std::setw((taille+3) * nb+2) << std::endl << std::setfill(' ');
  int Clm_DnL = (taille + 3) * nb + 1;

  for(int compteur = 0; compteur < Clm_DnL; ++compteur)
  {
    std::cout << '-' ;
  }

  std::cout << std::endl;
}

std::string Affichage_Donnee(char *info, unsigned int taille)
{
  std::string chaine;

  if(std::strlen(info) > taille)
  {
    for(unsigned int t = 0; t < taille - 3; t++)
    {
      chaine.push_back(info[t]);
    }

    chaine.append("...");
  }
  else
  {
    chaine.append(info);
  }

  return chaine;
}
